/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scrapingmercado.acceso;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class LecturaWEB {
    
    private final String URL = "https://listado.mercadolibre.com.co/";
    
    public Document obtenerHTML(String busqueda) {
        Document documento = null;
        try {
            documento = Jsoup.connect(URL + "/" + busqueda)
                    .userAgent("Mozilla")
                    .timeout(10000)
                    .get();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return documento;
    }
}
