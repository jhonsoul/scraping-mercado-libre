/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scrapingmercado.procesos;

import java.util.LinkedList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class OrganizaInformacion {
    
    private List<Producto> lista;

    /**
     * Genera una lista de productos con el contenido del documento html.
     * @param documento es el archivo html de la web digitalizado en java.
     */
    public void listaProductos(Document documento) {
        //Extrae la parte donde se muestra la lista de productos.
        Elements elementos = documento.select("div.ui-search-result__content-wrapper");
        lista = new LinkedList<>();
        elementos.forEach(elemento -> {
            try {
                Producto producto = new Producto();
                producto.setTitulo(elemento.select("h2.ui-search-item__title").text());
                producto.setUrlProducto(elemento.select("a").attr("abs:href"));
                producto.setPrecio(elemento.select("span.price-tag-fraction").text().split(" ")[0]);
                lista.add(producto);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
    }
    
    /**
     * Carga el contenido de la lista generadas en listaProductos en una tabla para ser mostrada.
     * @return entrega un modelo de tabla para ser mostrado en la interface.
     */
    public DefaultTableModel cargaTabla() {
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Número", "Título", "Precio", "URL"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        Object [] columnas;
        for (int i = 0; i < lista.size(); i++) {
            columnas = new Object[]{i + 1 ,lista.get(i).getTitulo(), lista.get(i).getPrecio(), lista.get(i).getUrlProducto()};
            modelo.addRow(columnas);
        }
        return modelo;
    }
}
